


 --DELETE FROM [psu0217_160722].[dbo].[Books];

 --Drop Table * [psu0217_160722].[dbo];
 
EXEC sp_msforeachtable "ALTER TABLE ? NOCHECK CONSTRAINT all"
Go

DECLARE @sql NVARCHAR(max)=''

SELECT @sql += ' Drop table ' + QUOTENAME(TABLE_SCHEMA) + '.'+ QUOTENAME(TABLE_NAME) + '; '
FROM   INFORMATION_SCHEMA.TABLES
WHERE  TABLE_TYPE = 'BASE TABLE'

Exec Sp_executesql @sql
