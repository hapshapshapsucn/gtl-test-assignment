﻿using System;
using System.Collections.Generic;
using System.Linq;
using Backend.Data;
using Backend.Logic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CopyController = WebApi.Controllers.CopyController;

namespace WebApi.Tests.Controllers
{
    [TestClass]
    public class CopyControllerIntegrationTest
    {
        private const string Book1Isbn13 = "9781408044056";
        private const string Book2Isbn10 = "1631409115";
        private const string Book1Title = "Foundations of Software Testing ISTQB Certification";
        private const string Book2Title = "Donald Duck - Timeless Tales";
        private int _copy1Id1;
        //private int _copy1Id2;
        //private int _copy2Id;
        private CopyController _copyController;
        private Backend.Logic.IBookController _backendBookController;
        private Backend.Logic.ICopyController _backendCopyController;

        [TestInitialize]
        public void Setup()
        {
            Backend.Database.IFactory<Backend.Database.IDbContext> dbFactory = new Backend.Database.DbContextFactory(Backend.Database.ApplicationScenario.TestingPersistent);
            _backendBookController = new Backend.Logic.BookController(dbFactory);
            _backendCopyController = new Backend.Logic.CopyController(dbFactory, (IBookControllerInternal)_backendBookController);

            _backendBookController.CreateBook(Book1Isbn13, null, Book1Title, null, null, DateTime.Now.Year, 15);
            _backendBookController.CreateBook(null, Book2Isbn10, Book2Title, null, null, DateTime.Now.Year, 15);
            _copy1Id1 = _backendCopyController.CreateCopy(Book1Isbn13).Id;
            //_copy1Id2 = _backendCopyController.CreateCopy(Book1Isbn13).Id;
            //_copy2Id = _backendCopyController.CreateCopy(Book2Isbn10).Id;

            _copyController = new CopyController(_backendCopyController);
        }

        [TestMethod]
        public void GetCopy_Returns_A_Copy()
        {
            // Arrange

            // Act
            Copy copy = _copyController.GetCopy(_copy1Id1);

            // Assert
            Assert.IsNotNull(copy);
        }

        [TestMethod]
        public void GetCopies_Returns_A_Set_Of_Copies()
        {
            // Arrange
            int bookId = _backendBookController.GetBook(Book1Isbn13).Id;

            // Act
            IEnumerable<Copy> copies = _copyController.GetCopies(bookId);

            // Assert
            Assert.IsTrue(copies.Any());
        }
    }
}
