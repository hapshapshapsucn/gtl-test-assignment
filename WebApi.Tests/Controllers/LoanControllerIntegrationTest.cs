﻿using System;
using Backend.Data;
using Backend.Logic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LoanController = WebApi.Controllers.LoanController;

namespace WebApi.Tests.Controllers
{
    [TestClass]
    public class LoanControllerIntegrationTest
    {
        private readonly Professor _professor = new Professor
        {
            SSN = "2uj46ute4ud",
            Adress = "1407 Graymalkin Lane, Salem Center",
            Campus = "The Xavier's School for Gifted Youngsters (X-Mansion)",
            Name = "Professor Charles Francis Xavier",
            PhoneNumber = "212-576-4000"
        };
        private const string Book1Isbn13 = "9781408044056";
        private const string Book2Isbn10 = "1631409115";
        private const string Book1Title = "Foundations of Software Testing ISTQB Certification";
        private const string Book2Title = "Donald Duck - Timeless Tales";
        private int _copy1Id1;
        //private int _copy1Id2;
        private int _copy2Id;
        private Backend.Logic.ILoanController _backendLoanController;
        private LoanController _loanController;

        [TestInitialize]
        public void Setup()
        {
            Backend.Database.IFactory<Backend.Database.IDbContext> dbFactory = new Backend.Database.DbContextFactory(Backend.Database.ApplicationScenario.TestingPersistent);
            Backend.Logic.IMemberController backendMemberController = new Backend.Logic.MemberController(dbFactory);
            Backend.Logic.IBookController backendBookController = new Backend.Logic.BookController(dbFactory);
            Backend.Logic.ICopyController backendCopyController = new Backend.Logic.CopyController(dbFactory, (Backend.Logic.IBookControllerInternal) backendBookController);
            _backendLoanController = new Backend.Logic.LoanController(dbFactory, (Backend.Logic.ICopyControllerInternal)backendCopyController, (IMemberControllerInternal) backendMemberController);
            
            backendMemberController.CreateMember(_professor.SSN, _professor.Name, _professor.Campus, _professor.Adress, _professor.PhoneNumber);
            backendBookController.CreateBook(Book1Isbn13, null, Book1Title, null, null, DateTime.Now.Year, 15);
            backendBookController.CreateBook(null, Book2Isbn10, Book2Title, null, null, DateTime.Now.Year, 15);
            _copy1Id1 = backendCopyController.CreateCopy(Book1Isbn13).Id;
            //_copy1Id2 = backendCopyController.CreateCopy(Book1Isbn13).Id;
            _copy2Id = backendCopyController.CreateCopy(Book2Isbn10).Id;

            _loanController = new LoanController(backendLoanController: _backendLoanController);
        }

        [TestMethod]
        public void GetLoan_By_LoanId_Return_A_Loan()
        {
            // Arrange
            Loan arrangeLoan = _backendLoanController.CreateLoan(_copy1Id1, _professor.SSN);

            // Act
            Loan loan = _loanController.GetLoan(arrangeLoan.LoanNumber);

            // Assert
            Assert.IsNotNull(loan);
        }

        [TestMethod]
        public void GetLoan_By_CopyId_Return_A_Loan()
        {
            // Arrange
            _backendLoanController.CreateLoan(_copy2Id, _professor.SSN);

            // Act
            Loan loan = _loanController.GetLoanByCopyId(_copy2Id);

            // Assert
            Assert.IsNotNull(loan);
        }
    }
}
