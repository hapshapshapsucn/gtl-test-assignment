﻿using System;
using System.Collections.Generic;
using System.Linq;
using Backend.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BookController = WebApi.Controllers.BookController;

namespace WebApi.Tests.Controllers
{
    [TestClass]
    public class BookControllerIntegrationTest
    {
        private const string Book1Isbn13 = "9781408044056";
        private const string Book2Isbn10 = "1631409115";
        private const string Book1Title = "Foundations of Software Testing ISTQB Certification";
        private const string Book2Title = "Donald Duck - Timeless Tales";
        private BookController _bookController;

        [TestInitialize]
        public void Setup()
        {
            Backend.Database.IFactory<Backend.Database.IDbContext> dbFactory = new Backend.Database.DbContextFactory(Backend.Database.ApplicationScenario.TestingPersistent);
            Backend.Logic.IBookController backendBookController = new Backend.Logic.BookController(dbFactory);
            backendBookController.CreateBook(Book1Isbn13, null, Book1Title, null, null, DateTime.Now.Year, 15);
            backendBookController.CreateBook(null, Book2Isbn10, Book2Title, null, null, DateTime.Now.Year, 15);
            _bookController = new BookController(backendBookController);
        }

        [TestMethod]
        public void GetBook_Return_The_Book()
        {
            // Arrange

            // Act
            Book book = _bookController.GetBook(Book1Isbn13);

            // Assert
            Assert.AreEqual(Book1Isbn13, book.Isbn13);
        }

        [TestMethod]
        public void GetBookAmount_Return_Set_Of_Book()
        {
            // Arrange

            // Act
            IEnumerable<Book> books = _bookController.GetBooksAmount(2);

            // Assert
            Assert.AreEqual(2, books.Count());
        }

        [TestMethod]
        public void FindBooks_Returns_A_set_of_matching_books()
        {
            // Arrange

            // Act
            IEnumerable<Book> books = _bookController.FindBooks("e");

            // Assert
            Assert.IsTrue(books.Any());
        }
    }
}
