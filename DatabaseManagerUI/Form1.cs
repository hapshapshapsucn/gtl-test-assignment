﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Backend.Data;
using Backend.Database;
using Backend.Logic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
// ReSharper disable LocalizableElement

namespace DatabaseManagerUI
{
    public partial class Form1 : Form
    {
        private string _file;
        private DateTime _startTime;
        private bool _processing;
        private int _processAmount;
        private int _countBookProcessed;
        private int _countBookFailed;
        private int _countCopies;

        public Form1()
        {
            InitializeComponent();
        }

        private void buttonSelectFile_Click(object sender, EventArgs e)
        {
            if (selectFileDialog.ShowDialog() == DialogResult.OK && (_file = selectFileDialog.FileName) != null)
            {

            }
        }

        private async void ButtonStartProcessing_Click(object sender, EventArgs e)
        {
            if (_processing) return;
            _processing = true;
            _startTime = DateTime.Now;
            buttonStartProcessing.Text = "Running..";
            var updateUi = UpdateUi();
            var dataprocess = ProcessDataFile();
            await dataprocess;
            await updateUi;
        }

        private async Task UpdateUi()
        {
            while (_processing)
            {
                var time = (DateTime.Now - _startTime);
                labelTime.Text = "Time: " + time.Minutes + @":" + time.Seconds;
                labelProcessedObjects.Text = _countBookProcessed + " items. Failed: " + _countBookFailed;
                labelTotalAdded.Text = "Total Added: Books: " + (_countBookProcessed - _countBookFailed) + @"/" +
                                       _processAmount + " Copies: " + _countCopies + @"/" + (_processAmount * 3);
                await Task.Delay(500);
            }
        }

        private async Task ProcessDataFile()
        {
            if (!String.IsNullOrEmpty(_file) && !String.IsNullOrEmpty(textBoxConnectionString.Text) && !String.IsNullOrEmpty(textGetAmount.Text))
            {
                _processAmount = Int32.Parse(textGetAmount.Text);
                string connectionString = textBoxConnectionString.Text;
                buttonStartProcessing.Text = "Connecting..";
                using (EfDbContext db = new EfDbContext(connectionString))
                {
                    if (db.Books.Any()) throw new Exception("Book table not removed from database.");
                    db.Configuration.AutoDetectChangesEnabled = false;
                    await Task.Delay(1);
                    buttonStartProcessing.Text = "Adding People..";
                    await Task.Delay(1);
                    await AddPeople(db);
                    buttonStartProcessing.Text = "Adding Books..";
                    await Task.Delay(1);
                    await AddBooks(db);
                    buttonStartProcessing.Text = "Adding copies..";
                    await Task.Delay(1);
                    await AddCopies(db);
                }
                buttonStartProcessing.Text = "Done!";
            }
            _processing = false;
        }

        private async Task AddPeople(EfDbContext db)
        {
            db.Members.Add(new Member
            {
                SSN = "98erihfd89d",
                Adress = "1407 Graymalkin Lane, Salem Center",
                Campus = "The Xavier's School for Gifted Youngsters (X-Mansion)",
                Name = "Scott Summers (Cyclops)",
                PhoneNumber = "212-576-4000"
            });
            db.Members.Add(new Member
            {
                SSN = "6857tjterd0fj9d",
                Adress = "1407 Graymalkin Lane, Salem Center",
                Campus = "The Xavier's School for Gifted Youngsters (X-Mansion)",
                Name = "Jean Grey (Phoenix)",
                PhoneNumber = "212-576-4000"
            });
            db.Members.Add(new Member
            {
                SSN = "5u6bjr9sbgrd89d",
                Adress = "1407 Graymalkin Lane, Salem Center",
                Campus = "The Xavier's School for Gifted Youngsters (X-Mansion)",
                Name = "James 'Jimmy' Howlett (Logan, Wolverine)",
                PhoneNumber = "212-576-4000"
            });
            db.Professors.Add(new Professor
            {
                SSN = "2uj46ute4ud",
                Adress = "1407 Graymalkin Lane, Salem Center",
                Campus = "The Xavier's School for Gifted Youngsters (X-Mansion)",
                Name = "Professor Charles Francis Xavier",
                PhoneNumber = "212-576-4000"
            });
            buttonStartProcessing.Text = "Saving People Changes..";
            await Task.Delay(1);
            await db.SaveChangesAsync();
        }

        private async Task AddBooks(EfDbContext db)
        {
            IBookControllerInternal bookController = new BookController(null);
            
            foreach (var book in DeserializeJsonFile(_file))
            {
                if (_countBookProcessed - _countBookFailed > _processAmount -1)
                {
                    break;
                }
                if (book == null || !BookToDB(book, bookController, db))
                {
                    _countBookFailed++;
                }
                _countBookProcessed++;
                await Task.Delay(1);
            }
            buttonStartProcessing.Text = "Saving Book Changes..";
            await db.SaveChangesAsync();
        }

        private async Task AddCopies(EfDbContext db)
        {
            foreach (var book in db.Books)
            {
                db.Copys.Add(new Copy { Book = book });
                db.Copys.Add(new Copy { Book = book });
                db.Copys.Add(new Copy { Book = book });
                _countCopies +=3;
                await Task.Delay(1);
            }
            buttonStartProcessing.Text = "Saving Copy Changes..";
            await db.SaveChangesAsync();
        }

        private IEnumerable<Book> DeserializeJsonFile(string file)
        {
            using (var fileReader = new StreamReader(file))
            using (var jsonReader = new JsonTextReader(fileReader))
            {
                //var serializer = new JsonSerializer();
                while (jsonReader.Read())
                {
                    Book book = new Book();
                    try
                    {
                        if (jsonReader.TokenType != JsonToken.StartObject) // Makes it go past all the unrelated list index information.
                            continue;

                        //var jsonObject = serializer.Deserialize(jsonReader);
                        var jsonObject = JToken.ReadFrom(jsonReader);

                        book.Isbn13 = jsonObject["isbn_13"]?.ToString();
                        book.Isbn10 = jsonObject["isbn_10"]?.ToString();
                        book.Title = jsonObject["title"]?.ToString();
                        book.SubTitle = jsonObject["subtitle"]?.Value<string>();
                        int dateInt;
                        if (Int32.TryParse(jsonObject["publish_date"]?.ToString(), out dateInt))
                            book.PublicationYear = dateInt;
                        int pageCountInt;
                        if (Int32.TryParse(jsonObject["number_of_pages"]?.Value<string>(), out pageCountInt))
                            book.PageCount = pageCountInt;
                        //DateTime date;
                        //if (DateTime.TryParse(jsonObject["publish_date"]?.Value<string>(), out date))
                        //    book.PublicationYear = date.Year;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Reading Book from json failed. Error: ", e);
                        book = null;
                    }
                    yield return book;
                }
            }
        }

        private bool BookToDB(Book book, IBookControllerInternal bookController, EfDbContext dbContext)
        {
            if (book == null)
                return false;
            var createdBook = bookController.CreateBook(dbContext, book.Isbn13, book.Isbn10, book.Title, book.SubTitle, null, book.PublicationYear, book.PageCount);
            if (createdBook == null)
                return false;
            var dbEntry = dbContext.Entry(createdBook);
            if (dbEntry.GetValidationResult().IsValid)
            {
                return true;
            }
            dbEntry.State = EntityState.Detached;
            MessageBox.Show(createdBook.ToString());
            return false;
        }

        private void textBoxConnectionString_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
