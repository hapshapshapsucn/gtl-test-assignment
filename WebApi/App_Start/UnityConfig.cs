using Microsoft.Practices.Unity;
using System.Web.Http;
using Unity.WebApi;

namespace WebApi
{
    public static class UnityConfig
    {
        private static UnityContainer _container;

        #region Unity Container
        //private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        //{
        //    var container = new UnityContainer();
        //    RegisterTypes(container);
        //    return container;
        //});

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            if(_container == null)
                RegisterComponents();
            return _container;
            //return container.Value;
        }
        #endregion

        public static void RegisterComponents()
        {
			_container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();
            _container.RegisterInstance(typeof(HttpConfiguration), GlobalConfiguration.Configuration);
            Backend.BackendManager _backendManager = new Backend.BackendManager();
            _container.RegisterInstance(typeof(Backend.Logic.IBookController), _backendManager.BookController);
            _container.RegisterInstance(typeof(Backend.Logic.ICopyController), _backendManager.CopyController);
            _container.RegisterInstance(typeof(Backend.Logic.ILoanController), _backendManager.LoanController);
            _container.RegisterInstance(typeof(Backend.Logic.IMemberController), _backendManager.MemberController);

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(_container);
        }
    }
}