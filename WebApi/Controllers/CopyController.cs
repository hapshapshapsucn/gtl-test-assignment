﻿using System.Collections.Generic;
using System.Web.Http;
using Backend.Data;

namespace WebApi.Controllers
{
    public class CopyController : ApiController
    {
        private readonly Backend.Logic.ICopyController _copyController;

        public CopyController(Backend.Logic.ICopyController backendCopyController)
        {
            _copyController = backendCopyController;
        }

        /// <summary>
        /// Gets a given copy by its copy id.
        /// </summary>
        /// <param name="copyId">The copy id to look for</param>
        /// <returns>The copy with the given id</returns>
        public Copy GetCopy(int copyId)
        {
            return _copyController.GetCopy(copyId);
        }

        /// <summary>
        /// Gets all copies, that is related to the given book.
        /// </summary>
        /// <param name="bookId">The book isbn to search with</param>
        /// <returns>All related copies</returns>
        public IEnumerable<Copy> GetCopies(int bookId)
        {
            return _copyController.GetCopies(bookId);
        }
    }
}
