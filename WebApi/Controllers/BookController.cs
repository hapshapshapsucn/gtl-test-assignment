﻿using System.Collections.Generic;
using System.Web.Http;
using Backend.Data;

namespace WebApi.Controllers
{
    public class BookController : ApiController
    {
        private readonly Backend.Logic.IBookController _bookController;

        public BookController(Backend.Logic.IBookController backendBookController)
        {
            _bookController = backendBookController;
        }

        /// <summary>
        /// Gets the book with the given isbn.
        /// </summary>
        /// <param name="isbn">The books isbn number</param>
        /// <returns>The mathing book</returns>
        public Book GetBook(string isbn)
        {
            return _bookController.GetBook(isbn);
        }

        /// <summary>
        /// Gets a set of books
        /// </summary>
        /// <param name="amount">The amount of books to get</param>
        /// <returns>A set of books</returns>
        public IEnumerable<Book> GetBooksAmount(int amount)
        {
            return _bookController.GetBooksAmount(amount);
        }

        /// <summary>
        /// Finds all books with a title that partly matches the parameter
        /// </summary>
        /// <param name="searchTitle">The part of a title to look for</param>
        /// <returns>All books with a partly matching title</returns>
        public IEnumerable<Book> FindBooks(string searchTitle)
        {
            return _bookController.GetBooks(searchTitle);
        }
    }
}
