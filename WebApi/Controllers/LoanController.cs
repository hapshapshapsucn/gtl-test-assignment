﻿using System.Collections.Generic;
using System.Web.Http;
using Backend.Data;

namespace WebApi.Controllers
{
    public class LoanController : ApiController
    {
        private readonly Backend.Logic.ILoanController _loanController;

        public LoanController(Backend.Logic.ILoanController backendLoanController)
        {
            _loanController = backendLoanController;
        }

        /// <summary>
        /// Gets the loan with the given id.
        /// </summary>
        /// <param name="loanId">The loan id to look for</param>
        /// <returns>The loan with the given id</returns>
        public Loan GetLoan(int loanId)
        {
            return _loanController.GetLoan(loanId);
        }

        /// <summary>
        /// Gets a list of loans, that have been made by a given person
        /// </summary>
        /// <param name="ssn">The persons ssn number</param>
        /// <returns>A list of loans</returns>
        public IEnumerable<Loan> GetLoans(string ssn)
        {
            return _loanController.GetLoans(ssn);
        }

        /// <summary>
        /// Gets the active loan with the given copyid which is loaned out.
        /// </summary>
        /// <param name="copyId">The copy id</param>
        /// <returns>The active loan with the copyid</returns>
        public Loan GetLoanByCopyId(int copyId)
        {
            return _loanController.GetLoanByCopyId(copyId);
        }

        /// <summary>
        /// Gets all copies related to a specific book, which isn't currently loaned out.
        /// </summary>
        /// <param name="bookIsbn">The books isbn</param>
        /// <returns>The copies for the given book, which isn't loaned out</returns>
        public IEnumerable<Copy> GetCopiesWithoutActiveLoan(string bookIsbn)
        {
            return _loanController.GetCopysWithoutActiveLoan(bookIsbn);
        }
    }
}