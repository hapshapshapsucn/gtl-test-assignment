﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Backend.Data;
using Website.Models;
using Backend.Logic;


namespace Website.Controllers
{
    public class BookController : Controller
    {
        private readonly IBookController _bookController;
        private readonly ICopyController _copyController;
        private readonly ILoanController _loanController;
        private string ifNullAuthor = "Stan Lee";

        public BookController(IBookController bookController, ICopyController copyController, ILoanController loanController)
        {
            _bookController = bookController;
            _copyController = copyController;
            _loanController = loanController;
        }

        public ActionResult Index(string searchTitle)
        {
            List<Backend.Data.Book> books = new List<Book>();
            //string searchTitle = Request["searchTitle"];
            books = string.IsNullOrEmpty(searchTitle) ? _bookController.GetBooksAmount(10) : _bookController.GetBooks(searchTitle);
            //List<Backend.Data.Book> books = _backendManager.BookController.GetBooks(searchTitle);
            List<VmBook> vmBooks = new List<VmBook>(books.Count);
            foreach (var book in books)
            {
                int copyCount = _copyController.CountCopies(book.Id);
                int copyCountNotLoaned = _loanController.CountCopysWithoutActiveLoan(book.Isbn10);
                string isbn = book.Isbn13 ?? book.Isbn10;
                string author = book?.Author?.FirstName + " " + book?.Author?.LastName;
                if (String.IsNullOrEmpty(author))
                    author = ifNullAuthor;
                var vmBook = new VmBook
                {
                    Isbn = isbn,
                    Title = book.Title,
                    CopyCount = copyCount,
                    CopyCountNotLoaned = copyCountNotLoaned,
                    Author = author
                };
                vmBooks.Add(vmBook);
            }
            return View(vmBooks);
        }

        // GET: Book
        //public ActionResult Index()
        //{
        //List<Backend.Data.Book> books = _backendManager.BookController.GetAllBooks();
        //List<VMBook> vmBooks = new List<VMBook>(books.Count);
        //foreach (var book in books)
        //{
        //    int copyCount = _backendManager.CopyController.GetCopies(book.Isbn10).Count;
        //    int copyCountNotLoaned = _backendManager.LoanController.GetCopysWithoutActiveLoan(book.Isbn10).Count;
        //    var vmBook = new VMBook
        //    {
        //        Isbn10 = book.Isbn10,
        //        Title = book.Title,
        //        CopyCount = copyCount,
        //        CopyCountNotLoaned = copyCountNotLoaned
        //    };
        //    vmBooks.Add(vmBook);
        //}
        //return View(/*vmBooks*/);
        //}

        //GET: Book/Details/*isbn*
        public ActionResult Details(string isbn)
        {
            VmBook vmBook = null;
            var book = _bookController.GetBook(isbn);
            if (book != null)
            {
                var copies = _copyController.GetCopies(book.Id);
                List<VmCopy> vmCopies = null;
                if (copies != null)
                {
                    vmCopies = new List<VmCopy>();
                    foreach (var copy in copies)
                    {
                        var loan = _loanController.GetLoanByCopyId(copy.Id);
                        VmCopy vmCopy = new VmCopy
                        {
                            Id = copy.Id,
                            IsLoaned = !loan?.LoanDone ?? false
                        };
                        vmCopies.Add(vmCopy);
                    }
                }
                vmBook = new VmBook
                {
                    Isbn = book.Isbn13 ?? book.Isbn10,
                    Title = book.Title,
                    Copies = vmCopies,
                    CopyCount = vmCopies != null ? vmCopies.Count : 0,
                    Author = String.IsNullOrWhiteSpace(book?.Author?.FirstName) ? ifNullAuthor : book?.Author?.FirstName
                };
            }
            return View(vmBook);
        }

        [HttpPost]
        public JsonResult GetBooksAjax(string searchTitle)
        {

            List<Backend.Data.Book> books = _bookController.GetBooks(searchTitle);
            List<VmBook> vmBooks = new List<VmBook>(books.Count);
            foreach (var book in books)
            {
                int copyCount = _copyController.GetCopies(book.Id).Count;
                int copyCountNotLoaned = _loanController.GetCopysWithoutActiveLoan(book.Isbn10).Count;
                var vmBook = new VmBook
                {
                    Isbn = book.Isbn10,
                    Title = book.Title,
                    CopyCount = copyCount,
                    CopyCountNotLoaned = copyCountNotLoaned
                };
                vmBooks.Add(vmBook);
            }

            //var json = new JavaScriptSerializer().Serialize(books);

            return Json(vmBooks);
        }
    }
}
