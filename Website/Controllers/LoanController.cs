﻿using System.Collections.Generic;
using System.Web.Mvc;
using Backend.Logic;
using Website.Models;

namespace Website.Controllers
{
    public class LoanController : Controller
    {
        private readonly string MemberSSN = "98erihfd89d";

        private readonly IBookController _bookController;
        private readonly ICopyController _copyController;
        private readonly ILoanController _loanController;

        public LoanController(IBookController bookController, ICopyController copyController, ILoanController loanController)
        {
            _bookController = bookController;
            _copyController = copyController;
            _loanController = loanController;
        }

        // GET: Loan
        public ActionResult Index()
        {
            var loans = _loanController.GetLoans(MemberSSN);
            List<VmLoan> vmLoans = new List<VmLoan>();
            if (loans != null)
            {
                foreach (var loan in loans)
                {
                    var book = _bookController.GetBook(loan.LoanedCopy.Book.Id);
                    var vmBook = new VmBook
                    {
                        Title = book.Title
                    };
                    var vmLoan = new VmLoan
                    {
                        Id = loan.LoanNumber,
                        CreationDateTime = loan.CreationDateTime,
                        EndDateTime = loan.EndDateTime,
                        LoanDone = loan.LoanDone,
                        LoanedCopy = new VmCopy
                        {
                            Id = loan.LoanedCopy.Id,
                            VmBook = vmBook
                        }
                    };
                    vmLoans.Add(vmLoan);
                }
            }
            return View(vmLoans);
        }

        //// GET: Loan/Details/5
        //public ActionResult Details(int id)
        //{
        //    return View();
        //}

        // GET: Loan/Create
        public ActionResult Create(int copyId)
        {
            var vmLoan = CreateLoan(copyId);
            if (vmLoan != null)
            {
                return RedirectToAction("Index");
            }
            return View();
        }

        public ActionResult End(int loanNumber)
        {
            var vmLoan = EndLoan(loanNumber);
            if (vmLoan == true)
            {
                return RedirectToAction("Index");
            }
            return View();
        }

        private bool EndLoan(int loanNumber)
        {
            return _loanController.EndLoan(loanNumber);
        }

        //// POST: Loan/Create
        //[HttpPost]
        //public ActionResult Create(int copyId)
        ////public ActionResult Create(FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add insert logic here
        //        var vmLoan = CreateLoan(copyId);
        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        private VmLoan CreateLoan(int copyId)
        {
            var loan = _loanController.CreateLoan(copyId, MemberSSN);
            VmLoan vmLoan = null;
            if (loan != null)
            {
                var book = _bookController.GetBook(loan.LoanedCopy.Book.Id);
                var vmBook = new VmBook
                {
                    Isbn = book.Isbn13 ?? book.Isbn10
                };
                vmLoan = new VmLoan
                {
                    Id = loan.LoanNumber,
                    CreationDateTime = loan.CreationDateTime,
                    EndDateTime = loan.EndDateTime,
                    LoanedCopy = new VmCopy { Id = loan.LoanedCopy.Id, VmBook = vmBook, IsLoaned = true }
                };
            }
            return vmLoan;
        }

        //// GET: Loan/Edit/5
        //public ActionResult Edit(int id)
        //{
        //    return View();
        //}

        //// POST: Loan/Edit/5
        //[HttpPost]
        //public ActionResult Edit(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add update logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //// GET: Loan/Delete/5
        //public ActionResult Delete(int id)
        //{
        //    return View();
        //}

        //// POST: Loan/Delete/5
        //[HttpPost]
        //public ActionResult Delete(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add delete logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}
    }
}
