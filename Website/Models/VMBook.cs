﻿using System.Collections.Generic;
using System.ComponentModel;

namespace Website.Models
{
    public class VmBook
    {
        [DisplayName("ISBN")]
        public string Isbn { get; set; }
        public string Title { get; set; }
        [DisplayName("Copy count")]
        public int CopyCount { get; set; }
        public int CopyCountNotLoaned { get; set; }
        [DisplayName("Copies")]
        public string CopyCountPretty => this.CopyCountNotLoaned + "/" + this.CopyCount + " Available";

        [DisplayName("Auhtor")]
        public string Author { get; set; }
        public IEnumerable<VmCopy> Copies { get; set; }

        public VmBook()
        {
        }

        public VmBook(string isbn, string title, int copyCount, int copyCountNotLoaned, string author)
        {
            this.Isbn = isbn;
            this.Title = title;
            this.CopyCount = copyCount;
            this.CopyCountNotLoaned = copyCountNotLoaned;
            this.Author = author;
        }

        public VmBook(string isbn, string title, int copyCount, int copyCountNotLoaned, IEnumerable<VmCopy> copies, string author)
        {
            this.Isbn = isbn;
            this.Title = title;
            this.CopyCount = copyCount;
            this.CopyCountNotLoaned = copyCountNotLoaned;
            this.Copies = copies;
            this.Author = author;
        }
    }
}
