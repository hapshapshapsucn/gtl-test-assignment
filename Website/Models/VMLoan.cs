﻿using System;
using System.ComponentModel;

namespace Website.Models
{
    public class VmLoan
    {
        [DisplayName("Loan Id")]
        public int Id { get; set; }
        public DateTime CreationDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public  VmCopy LoanedCopy { get; set; }
        //public  Member Member { get; set; }
        public bool LoanDone { get; set; }
    }
}