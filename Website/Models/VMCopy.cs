﻿using System.ComponentModel;

namespace Website.Models
{
    public class VmCopy
    {
        [DisplayName("Copy Id")]
        public int Id { get; set; }
        [DisplayName("Book")]
        public VmBook VmBook { get; set; }
        [DisplayName("Loan status")]
        public bool IsLoaned { get; set; }
    }
}
