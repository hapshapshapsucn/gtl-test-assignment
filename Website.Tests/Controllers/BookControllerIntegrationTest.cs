﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Backend;
using Backend.Logic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Website.Models;
using BackendManager = Backend.BackendManager;
using BookController = Website.Controllers.BookController;

// ReSharper disable PossibleNullReferenceException
// ReSharper disable AssignNullToNotNullAttribute

namespace Website.Tests.Controllers
{
    [TestClass]
    public class BookControllerIntegrationTest
    {
        private const string Book1Isbn13 = "9781408044056";
        private const string Book2Isbn10 = "1631409115";
        private const string Book1Title = "Foundations of Software Testing ISTQB Certification";
        private const string Book2Title = "Donald Duck - Timeless Tales";
        //private int _copy1Id1;
        //private int _copy1Id2;
        //private int _copy2Id;
        private Backend.Logic.IBookController _backendBookController;
        private BookController _bookController;

        [TestInitialize]
        public void Setup()
        {
            Backend.Database.IFactory<Backend.Database.IDbContext> dbFactory = new Backend.Database.DbContextFactory(Backend.Database.ApplicationScenario.TestingPersistent);
            Backend.Logic.IMemberController backendMemberController = new Backend.Logic.MemberController(dbFactory);
            _backendBookController = new Backend.Logic.BookController(dbFactory);
            Backend.Logic.ICopyController backendCopyController = new Backend.Logic.CopyController(dbFactory, (Backend.Logic.IBookControllerInternal)_backendBookController);
            Backend.Logic.ILoanController backendLoanController = new Backend.Logic.LoanController(dbFactory, (Backend.Logic.ICopyControllerInternal)backendCopyController, (IMemberControllerInternal)backendMemberController);

            _backendBookController.CreateBook(Book1Isbn13, null, Book1Title, null, null, DateTime.Now.Year, 15);
            _backendBookController.CreateBook(null, Book2Isbn10, Book2Title, null, null, DateTime.Now.Year, 15);
            //_copy1Id1 = backendCopyController.CreateCopy(Book1Isbn13).Id;
            //_copy1Id2 = backendCopyController.CreateCopy(Book1Isbn13).Id;
            //_copy2Id = backendCopyController.CreateCopy(Book2Isbn10).Id;

            _bookController = new BookController(_backendBookController, backendCopyController, backendLoanController);
        }

        [TestMethod]
        public void Index_Model_Contains_Books()
        {
            // Arrange

            // Act
            ViewResult result = _bookController.Index(null) as ViewResult;
            var books = result.Model as IList<VmBook>;

            // Assert
            Assert.IsTrue(books.Any());
        }

        [TestMethod]
        public void Index_Model_Contain_Books_Matching_Given_Title()
        {
            // Arrange
            string title = _backendBookController.GetBooksAmount(1).First().Title;

            // Act
            ViewResult result = _bookController.Index(title) as ViewResult;
            var books = result.Model as IList<VmBook>;

            // Assert
            Assert.IsTrue(books.Any(book => book.Title == title));
        }

        [TestMethod]
        public void Details_Model_Contains_Book_Matching_Isbn()
        {
            // Arrange
            var backendBook = _backendBookController.GetBooksAmount(1).First();
            string isbn = backendBook.Isbn13 ?? backendBook.Isbn10;

            // Act
            ViewResult result = _bookController.Details(isbn) as ViewResult;
            var book = result.Model as VmBook;

            // Assert
            Assert.AreEqual(isbn, book.Isbn);
        }

        [TestMethod]
        public void Details_Model_Is_Null_When_No_Book_Matches_isbn()
        {
            // Arrange
            string isbn = Utility.RandomString(15);

            // Act
            ViewResult result = _bookController.Details(isbn) as ViewResult;
            var book = result.Model as VmBook;

            // Assert
            Assert.IsNull(book);
        }
    }
}
