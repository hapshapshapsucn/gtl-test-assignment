﻿using System;
using System.Collections.Generic;
using Backend.Data;
using Backend.Database;
using Backend.Logic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Backend.Tests.IntegrationTest
{
    [TestClass]
    public class TestLoan
    {
        private IFactory<IDbContext> _dbContextFactory;
        private ILoanController _loanController;
        private ICopyController _copyController;
        private IMemberController _memberController;
        private const string BookIsbn = "9781401242350";
        private const string BookTitle = "The Joker: Death of the Family (The New 52)";
        private const int CopyId = 16; // Charles Exavior graduated with honors at the age of 16 from Bard College.
        private const string MemberSSN = "676eri4sfsgfjfd89d";
        private const string Member2SSN = "6763436ryijfd89d";

        /// <summary>
        /// Initialize controllers
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            _dbContextFactory = new DbContextFactory(ApplicationScenario.TestingPersistent);
            IBookControllerInternal bookController = new BookController(_dbContextFactory);
            _copyController = new CopyController(_dbContextFactory, bookController);
            _memberController = new MemberController(_dbContextFactory);
            _loanController = new LoanController(_dbContextFactory, (ICopyControllerInternal) _copyController, (IMemberControllerInternal) _memberController);
        }

        #region TestSupportMethods
        /// <summary>
        /// Setup data method
        /// </summary>
        /// <returns></returns>
        private Tuple<Copy, Member, Professor> AddTestMemberAndCopyToDatabase()
        {
            IDbContext dbContext = _dbContextFactory.Create(); //Add a copy and member to db, to test with.
            Copy copy = dbContext.Copys.Add(new Copy
            {
                Book = new Book { Isbn13 = BookIsbn, Title = BookTitle}
            });
            Member member = dbContext.Members.Add(new Member
            {
                SSN = MemberSSN,
                Adress = "1407 Graymalkin Lane, Salem Center",
                Campus = "The Xavier's School for Gifted Youngsters (X-Mansion)",
                Name = "Scott Summers (Cyclops)",
                PhoneNumber = "212-576-4000"
            });
            Professor professor = dbContext.Members.Add(new Professor
            {
                SSN = Member2SSN,
                Adress = "1407 Graymalkin Lane, Salem Center",
                Campus = "The Xavier's School for Gifted Youngsters (X-Mansion)",
                Name = "Professor Charles Francis Xavier",
                PhoneNumber = "212-576-4000"
            }) as Professor;
            dbContext.SaveChanges();
            return new Tuple<Copy, Member, Professor>(copy, member, professor);
        }
        #endregion

        #region TestProcedures
        /// <summary>
        /// "Test Case 1. 
        /// Kriterie 1. Et lån kan foretages af et medlem"
        /// </summary>
        [TestMethod]
        public void Procedure_1_A_loan_can_be_made_by_A_Member() // Et lån kan foretages af et medlem
        {
            // Arrange
            Tuple<Copy, Member, Professor> objects = AddTestMemberAndCopyToDatabase();
            Member member = objects.Item2;
            Copy copy = objects.Item1;

            // Act
            Loan loan = _loanController.CreateLoan(loanedCopyId: copy.Id, memberSsn: member.SSN);

            // Assert
            Assert.IsNotNull(loan);
        }

        /// <summary>
        /// "Test Case 1. 
        /// Kriterie 2. Et lån kan foretages af en professor"
        /// </summary>
        [TestMethod]
        public void Procedure_2_A_loan_can_be_made_by_A_Professor() // Et lån kan foretages af en professor
        {
            // Arrange
            Tuple<Copy, Member, Professor> objects = AddTestMemberAndCopyToDatabase();
            Professor professor = objects.Item3;
            Copy copy = objects.Item1;

            // Act
            Loan loan = _loanController.CreateLoan(loanedCopyId: copy.Id, memberSsn: professor.SSN);

            // Assert
            Assert.IsNotNull(loan);
        }
        
        /// <summary>
        /// "Test Case 1. 
        /// Kriterie 3. Et lån foretages kun hvis bog eksemplaret findes."
        /// </summary>
        [TestMethod]
        public void Procedure_3_A_loan_can_only_be_made_if_copy_exists() // Et lån foretages kun hvis bog eksemplaret findes.
        {
            // Arrange
            Tuple<Copy, Member, Professor> objects = AddTestMemberAndCopyToDatabase();
            Member member = objects.Item2;
            int copyId = 16;

            // Act
            Loan loan = _loanController.CreateLoan(loanedCopyId: copyId, memberSsn: member.SSN);

            // Assert
            Assert.IsNull(loan);
        }

        /// <summary>
        /// "Test Case 1. 
        /// Kriterie 4. Et medlem kan ikke låne mere end 5 bog eksemplare
        /// Kriterie 5. En professor kan ikke låne mere end 5 bog eksemplare"
        /// </summary>
        [TestMethod]
        public void Procedure_4_Member_Can_Not_Loan_More_Than_Five_Copies()
        {
            // Arrange
            Tuple<Copy, Member, Professor> objects = AddTestMemberAndCopyToDatabase();
            Member member = objects.Item2;
            for (int i = 0; i < 5; i++)
            {
                Copy copy = _copyController.CreateCopy(objects.Item1.Book.Isbn13);
                Loan loan = _loanController.CreateLoan(copy.Id, member.SSN);
            }

            // Act
            Copy notNullCopy = _copyController.CreateCopy(objects.Item1.Book.Isbn13);
            Loan nullLoan = _loanController.CreateLoan(notNullCopy.Id, member.SSN);

            // Assert
            Assert.IsNull(nullLoan);
        }
        
        /// <summary>
        /// "Test Case 1. 
        /// Kriterie 6. Et lån bliver retuneret med det givne eksemplar, når et lån bliver foretaget med et bog eksemplar."
        /// </summary>
        [TestMethod]
        public void Procedure_5_New_Loan_Returns_Loan_With_Copy()
        {
            // Arrange
            Tuple<Copy, Member, Professor> objects = AddTestMemberAndCopyToDatabase();
            Member member = objects.Item2;
            Copy copy = objects.Item1;
            
            // Act
            Loan loan = _loanController.CreateLoan(copy.Id, member.SSN);

            // Assert
            Assert.AreEqual(copy.Id, loan.LoanedCopy.Id);
        }

        /// <summary>
        /// "Test Case 1. 
        /// Kriterie 7. Der retuneres null, hvis bog eksemplaret ikke kunne lånes."
        /// </summary>
        [TestMethod]
        public void Procedure_6_New_Loan_Returns_Loan_With_Copy()
        {
            // Arrange
            Tuple<Copy, Member, Professor> objects = AddTestMemberAndCopyToDatabase();
            Member member = objects.Item2;
            Copy copy = objects.Item1;
            _loanController.CreateLoan(copy.Id, member.SSN);

            // Act
            Loan loan = _loanController.CreateLoan(copy.Id, member.SSN);

            // Assert
            Assert.IsNull(loan);
        }

        #endregion

        [TestMethod]
        public void Create_Loan_Action_Adds_Loan_To_Database()
        {
            //Arrange
            Tuple<Copy, Member, Professor> objects = AddTestMemberAndCopyToDatabase();
            //Act
            Loan ctrLoan = _loanController.CreateLoan(objects.Item1.Id, objects.Item2.SSN);
            Loan dbLoan = _dbContextFactory.Create().Loans.Find(ctrLoan.LoanNumber);
            //Assert
            Assert.AreEqual(ctrLoan.LoanNumber, dbLoan.LoanNumber);
        }
    }
}
