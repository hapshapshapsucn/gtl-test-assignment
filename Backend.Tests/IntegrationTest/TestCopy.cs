﻿using Backend.Data;
using Backend.Database;
using Backend.Logic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Backend.Tests.IntegrationTest
{
    [TestClass]
    public class TestCopy
    {
        private IFactory<IDbContext> _dbContextFactory;
        private ICopyController _copyController;
        private const string BookIsbn = "0671629646";
        private const string BookTitle = "Hitchikers Guide to the galaxy";

        [TestInitialize]
        public void Initialize()
        {
            //Arrange
            _dbContextFactory = new DbContextFactory(ApplicationScenario.TestingPersistent);
            IBookControllerInternal bookController = new BookController(_dbContextFactory);
            _copyController = new CopyController(_dbContextFactory, bookController);
        }

        private void AddBookToDatabase()
        {
            IDbContext dbContext = _dbContextFactory.Create();
            dbContext.Books.Add(new Book
            {
                Title = BookTitle,
                Isbn10 = BookIsbn
            });
            dbContext.SaveChanges();
        }

        [TestMethod]
        public void Database_Generates_CopyId()
        {
            //Arrange
            AddBookToDatabase();
            //Act
            Copy copy1 = _copyController.CreateCopy(bookIsbn: BookIsbn);
            Copy copy2 = _copyController.CreateCopy(bookIsbn: BookIsbn);
            //Assert
            Assert.AreNotEqual(copy2.Id, copy1.Id);
        }
    }
}
