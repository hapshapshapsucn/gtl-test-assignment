﻿using System;
using System.Collections.Generic;
using Backend.Data;
using Backend.Database;
using Backend.Logic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Backend.Tests.IntegrationTest
{
    [TestClass]
    public class TestCopyController
    {
        private IFactory<IDbContext> _dbContextFactory;
        private ICopyController _copyController;
        private const string BookIsbn = "0671629646";
        private const string BookTitle = "Hitchikers Guide to the galaxy";

        [TestInitialize]
        public void Initialize()
        {
            _dbContextFactory = new DbContextFactory(ApplicationScenario.TestingPersistent);
            IBookControllerInternal bookController = new BookController(_dbContextFactory);
            _copyController = new CopyController(_dbContextFactory, bookController);
        }

        private Tuple<List<Copy>, Book> AddTestBookAndCopiesToDatabase()
        {
            IDbContext db = _dbContextFactory.Create();
            //db.Books.Where(b => b.Isbn10 == BookIsbn).ForEachAsync(b => db.Books.Remove(b));
            Book book = db.Books.Add(new Book
            {
                Title = BookTitle,
                Isbn10 = BookIsbn,
                PublicationYear = DateTime.Now.Year
            });
            int size = 3;
            var copies = new List<Copy>(size);
            for (int i = 0; i < size; i++)
            {
                var copy = db.Copys.Add(new Copy
                {
                    Book = book
                });
                copies.Add(copy);
            }
            db.SaveChanges();
            return new Tuple<List<Copy>, Book>(copies, book);
        }

        [TestMethod]
        public void GetCopies_Gets_Copies_For_Book()
        {
            //Arrange
            var objects = AddTestBookAndCopiesToDatabase();

            //Act
            var copies = _copyController.GetCopies(objects.Item2.Id);

            //Assert
            Assert.AreEqual(objects.Item1.Count, copies.Count);
        }
    }
}
