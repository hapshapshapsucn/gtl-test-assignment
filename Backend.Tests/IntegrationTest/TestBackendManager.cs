﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Backend.Tests.IntegrationTest
{
    [TestClass]
    public class TestBackendManager
    {
        private BackendManager _backendManager;

        [TestInitialize]
        public void Setup()
        {
            _backendManager = new BackendManager();
        }

        [TestMethod]
        public void Test_get_BookController_Is_Not_Null()
        {
            // Arrange

            // Act

            // Assert
            Assert.IsNotNull(_backendManager.BookController);
        }

        [TestMethod]
        public void Test_get_CopyController_Is_Not_Null()
        {
            // Arrange

            // Act

            // Assert
            Assert.IsNotNull(_backendManager.CopyController);
        }

        [TestMethod]
        public void Test_get_LoanController_Is_Not_Null()
        {
            // Arrange

            // Act

            // Assert
            Assert.IsNotNull(_backendManager.LoanController);
        }

        [TestMethod]
        public void Test_get_MemberController_Is_Not_Null()
        {
            // Arrange

            // Act

            // Assert
            Assert.IsNotNull(_backendManager.MemberController);
        }
    }
}
