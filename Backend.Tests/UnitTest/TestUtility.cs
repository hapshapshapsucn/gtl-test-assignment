﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Backend.Tests.UnitTest
{
    [TestClass]
    public class TestUtility
    {
        [TestMethod]
        public void TestRandomString()
        {
            Assert.AreNotEqual(Utility.RandomString(5), Utility.RandomString(5));
            Assert.AreNotEqual(Utility.RandomString(15), Utility.RandomString(15));
        }

        [TestMethod]
        [DataRow(true, "0671629646")] // ISBN 10, Hitchikers Guide to the galaxy
        [DataRow(true, "9781408044056")] // ISBN 13, Foundations of Software Testing ISTQB Certification
        [DataRow(true, " 0-671 - 62964-6 ")] // ISBN 10, Hitchikers Guide to the galaxy with dashes and spaces
        [DataRow(false, "9781408044055")] // ISBN 13, Foundations of Software Testing ISTQB Certification -- INVALID Checkcsum
        [DataRow(false, "978140804405")] // ISBN 13, missing number
        public void TestIsbnConvertion(bool expected, string isbn)
        {
            Assert.AreEqual(expected, Utility.ValidateIsbn(isbn));
        }

        [TestMethod]
        [DataRow(Utility.IsbnType.Isbn10, "0671629646")] // ISBN 10, Hitchikers Guide to the galaxy
        [DataRow(Utility.IsbnType.Isbn13, "9781408044056")] // ISBN 13, Foundations of Software Testing ISTQB Certification
        [DataRow(Utility.IsbnType.None, "9781408044055")] // ISBN 13, Foundations of Software Testing ISTQB Certification -- INVALID Checkcsum
        [DataRow(Utility.IsbnType.None, "978140804405")] // ISBN 13, missing number
        [DataRow(Utility.IsbnType.Isbn13, "9781631409110")] // ISBN 13, Donald Duck - Timeless Tales
        [DataRow(Utility.IsbnType.Isbn10, "0160581940")] // ISBN 10, EX-IM Bank oversight hearing. Represented in dataset as 016058194X
        public void TestGetIsbnType(Utility.IsbnType expected, string isbn)
        {
            Assert.AreEqual(expected, Utility.GetIsbnType(isbn));
        }

        [TestMethod]
        [DataRow("0160581940", "016058194X")]
        [DataRow("0160581940", "0160-r58-dfjso194X")]
        [DataRow(null, @"/\-/\ehgioejvdvojdx")]
        public void TestSimplifyIsbn(string expected, string isbn)
        {
            Assert.AreEqual(expected, Utility.SimplifyIsbn(isbn));
        }

        [TestMethod]
        [DataRow(1, 2, 3)]
        [DataRow(1, 3, 4)]
        [DataRow(4, 2, 6)]
        [DataRow(0, 2, 2)]
        public void TestSum(int a, int b, int exspected)
        {
            int result = a + b;
            Assert.AreEqual(exspected, result);
        }
    }
}
