﻿using System;
using Backend.Data;
using Backend.Database;
using Backend.Logic;
using Backend.Tests.Mocking;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Backend.Tests.UnitTest
{
    [TestClass]
    public class TestCrudBook
    {
        private const string BookIsbn = "0671629646";
        private const string BookTitle = "Hitchikers Guide to the galaxy";
        private const string NewBookIsbn = "1631409115";
        private const string NewBookTitle = "Donald Duck - Timeless Tales";
        private Mock<IFactory<IDbContext>> _mockIFactory;
        private Mock<IDbContext> _mockDbContext;
        private FakeDbSetMoq<Book> _fakeDbSet;
        private IBookController _bookController;
        private Book _book;

        [TestInitialize]
        public void Initialize()
        {
            _mockIFactory = new Mock<IFactory<IDbContext>>();
            _mockDbContext = new Mock<IDbContext>();
            
            _mockIFactory.Setup(factory => factory.Create()).Returns(_mockDbContext.Object);
            
            _bookController = new BookController(_mockIFactory.Object);

            _book = new Book
            {
                Isbn10 = BookIsbn,
                Title = BookTitle
            };
            _fakeDbSet = new FakeDbSetMoq<Book>
            {
                _book
            };
            _mockDbContext.Setup(context => context.Books).Returns(_fakeDbSet);
        }

        [TestCleanup]
        public void Cleanup()
        {
            //This is technically not needed, as these gets defined every time for every test, in the initializer.
            _mockIFactory = null;
            _mockDbContext = null;
            _bookController = null;
            _book = null;
        }

        #region Test_SaveChanges_MockTest

        [TestMethod]
        public void Generate_Book_Calls_SaveChanges()
        {
            //Arrange
            _mockDbContext.Setup(context => context.Books.Add(It.IsAny<Book>())).Returns(new Book()); //This is currently a Stub
            //Act
            _bookController.CreateBook("testMateral1234isbn13", null, "test Book", "test subTitle", null, DateTime.Now.Year, 14);
            //Assert
            _mockDbContext.Verify(context => context.SaveChanges(), Times.Once); //This is a Mock!
        }

        [TestMethod]
        public void Update_Book_Calls_SaveChanges()
        {
            //Arrange
            //_mockDbContext.Setup(context => context.Books.Find(BookIsbn)).Returns(_book); //This is currently a Stub
            //Act
            _bookController.UpdateBook(BookIsbn, NewBookIsbn, NewBookTitle, null, DateTime.Now);
            //Assert
            _mockDbContext.Verify(context => context.SaveChanges(), Times.Once); //This is a Mock!
        }

        [TestMethod]
        public void Remove_Book_Calls_SaveChanges()
        {
            //Arrange
            //_mockDbContext.Setup(context => context.Books.Find(BookIsbn)).Returns(_book); //This is currently a Stub
            //_mockDbContext.Setup(context => context.Books.Remove(_book)).Returns(_book); //This is currently a Stub
            //Act
            _bookController.RemoveBook(BookIsbn);
            //Assert
            _mockDbContext.Verify(context => context.SaveChanges(), Times.Once); //This is a Mock!
        }

        #endregion

        #region Positive Tests

        [TestMethod]
        public void Generate_Book_Positive()
        {
            //Arrange
            _mockDbContext.Setup(context => context.Books.Add(It.IsAny<Book>())).Returns(new Book()); //This is a stub
            //Act
            Book book = _bookController.CreateBook(BookIsbn, null, BookTitle, "subTitle", null, DateTime.Now.Year, 46);
            //Assert
            Assert.IsNotNull(book);
        }

        [TestMethod]
        public void Get_Book_Positive()
        {
            //_mockDbContext.Setup(context => context.Books.Find(BookIsbn)).Returns(_book);
            
            var result = _bookController.GetBook(BookIsbn);
            Assert.AreEqual(BookIsbn, result.Isbn10);
            Assert.AreEqual(BookTitle, result.Title);
        }

        [TestMethod]
        public void Update_Book_Positive()
        {
            //Arrange
            //_mockDbContext.Setup(context => context.Books.Find(BookIsbn)).Returns(_book);
            //Act
            var result = _bookController.UpdateBook(BookIsbn, NewBookIsbn, NewBookTitle, null, DateTime.Now);
            //Assert
            //_mockDbContext.Verify(context => context.SaveChanges(), Times.Once);
            Assert.AreEqual(NewBookIsbn, result.Isbn10);
            Assert.AreEqual(NewBookTitle, result.Title);
        }

        [TestMethod]
        public void Remove_Book_Positive()
        {
            //Arrange
            //_mockDbContext.Setup(context => context.Books.Find(BookIsbn)).Returns(_book);
            //_mockDbContext.Setup(context => context.Books.Remove(_book)).Returns(_book);
            //Act
            var result = _bookController.RemoveBook(BookIsbn);
            //Assert
            Assert.IsTrue(result);
        }

        #endregion


        #region Negative Tests

        //[TestMethod]
        //public void Generate_Book_Negative()
        //{
        //    throw new NotImplementedException();
        //    _mockDbContext.Setup(context => context.Books.Add(It.IsAny<Book>())).Returns(new Book());


        //    Book Book = _BookController.CreateBook("testMateral1234", "test Book",
        //        new Author { FirstName = "Jens", MiddleName = "Kristian", LastName = "Kristensen" }, DateTime.Now);
        //    Assert.IsNotNull(Book);
        //    Assert.IsInstanceOfType(Book, typeof(Book));
        //}

        [TestMethod]
        public void Get_Booke_Negative()
        {
            //_mockDbContext.Setup(context => context.Books.Find(BookIsbn)).Returns((Book)null);
            Book result = _bookController.GetBook("45768");

            Assert.IsNull(result);
        }

        //[TestMethod]
        //public void Update_Book_Negative() //Tests that you can't change the id to one that already exists.
        //{
        //    string newBookId = "w4w98y3iu";
        //    string newBookTitle = "testingALittleTitleChange";

        //    _mockDbContext.Setup(context => context.Books.Find(BookId)).Returns(_Book);
        //    var result = _BookController.UpdateBook(BookId, newBookId, newBookTitle, null, DateTime.Now);
        //    Assert.IsNotNull(result);
        //    Assert.AreEqual(newBookId, result.Id);
        //    Assert.AreNotEqual(BookId, result.Id);
        //    Assert.AreEqual(newBookTitle, result.Title);
        //    Assert.AreNotEqual(BookTitle, result.Id);
        //}

        //[TestMethod]
        //public void Remove_Book_Negative()
        //{
        //    throw new NotImplementedException();
        //    _mockDbContext.Setup(context => context.Books.Find(BookId)).Returns(_Book);
        //    _mockDbContext.Setup(context => context.Books.Remove(_Book)).Returns(_Book);
        //    var result = _BookController.RemoveBook(BookId);
        //    Assert.IsTrue(result);
        //}

        #endregion

    }
}
