﻿using System;
using Backend.Data;
using Backend.Database;
using Backend.Logic;
using Backend.Tests.Mocking;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Backend.Tests.UnitTest
{
    [TestClass]
    public class TestLoanController
    {
        private const int LoanNumber = 8974480;
        private const int CopyId = 89235240;
        private const string MemberSsn = "hiop34rdfsf9089";

        private Mock<IDbContext> _mockDbContext;
        private Mock<IFactory<IDbContext>> _mockIFactory;
        private ILoanControllerInternal _loanController;
        private Mock<ICopyControllerInternal> _mockCopyController;
        private Mock<IMemberControllerInternal> _mockMemberController;
        private Mock<FakeLoanSet> _mockFakeDbSet;
        private Loan _loanedLoan;
        private Copy _copy;
        private Member _member;

        [TestInitialize]
        public void Initialize()
        {
            //Arrange
            _mockIFactory = new Mock<IFactory<IDbContext>>();
            _mockDbContext = new Mock<IDbContext>();
            _mockIFactory.Setup(factory => factory.Create()).Returns(_mockDbContext.Object);
            _mockCopyController = new Mock<ICopyControllerInternal>();
            _mockMemberController = new Mock<IMemberControllerInternal>();
            _loanController = new LoanController(_mockIFactory.Object, _mockCopyController.Object, _mockMemberController.Object);
            _mockFakeDbSet = new Mock<FakeLoanSet> {CallBase = true};
            _mockDbContext.Setup(context => context.Loans).Returns(_mockFakeDbSet.Object);
            
            _copy = new Copy
            {
                Id = CopyId
            };

            _member = new Member
            {
                SSN = MemberSsn
            };

            _loanedLoan = new Loan
            {
                CreationDateTime = DateTime.Now,
                EndDateTime = DateTime.Now.AddDays(1),
                LoanNumber = LoanNumber,
                LoanedCopy = _copy,
                Member = _member
            };
            _mockFakeDbSet.Object.Local.Add(_loanedLoan);
        }

        [TestCleanup]
        public void Cleanup()
        {
            //This is technically not needed, as these gets defined every time for every test, in the initializer.
            _mockIFactory = null;
            _mockDbContext = null;
            _loanController = null;
            _mockCopyController = null;
            _mockMemberController = null;
            _loanedLoan = null;
            _copy = null;
            _member = null;
            _mockFakeDbSet = null;
        }

        #region Test_SaveChanges
        [TestMethod]
        public void CreateLoan_Calls_SaveChanges()
        {
            //Arrange
            const int localCopyId = 2587;
            Copy copy = new Copy { Id = localCopyId };
            _mockCopyController.Setup(stuff => stuff.GetCopy(It.IsAny<IDbContext>(), localCopyId)).Returns(copy);
            _mockMemberController.Setup(stuff => stuff.GetMember(It.IsAny<IDbContext>(), MemberSsn)).Returns(_member);

            //Act
            _loanController.CreateLoan(localCopyId, MemberSsn);
            //Assert
            _mockDbContext.Verify(context => context.SaveChanges(), Times.Once);
        }

        #endregion

        [TestMethod]
        public void CreateLoan_Adds_Loan_To_database()
        {
            //Arrange
            const int localCopyId = 2587;
            Copy copy = new Copy{ Id = localCopyId };
            _mockCopyController.Setup(stuff => stuff.GetCopy(It.IsAny<IDbContext>(), localCopyId)).Returns(copy);
            _mockMemberController.Setup(stuff => stuff.GetMember(It.IsAny<IDbContext>(), MemberSsn)).Returns(_member);

            //Act
            _loanController.CreateLoan(localCopyId, MemberSsn);

            //Assert
            _mockFakeDbSet.Verify(context => context.Add(It.IsAny<Loan>()), Times.Once);
        }

        [TestMethod]
        public void GetLoan_Returns_A_Loan()
        {
            //Arrange
            _mockDbContext.Setup(context => context.Loans.Find(It.IsAny<int>())).Returns(_loanedLoan);
            _mockCopyController.Setup(copy => copy.GetCopy(It.IsAny<IDbContext>(), It.IsAny<int>())).Returns(_copy);
            //Act
            Loan result = _loanController.GetLoan(LoanNumber);
            //Assert
            Assert.IsNotNull(result);
        }
    }
}
