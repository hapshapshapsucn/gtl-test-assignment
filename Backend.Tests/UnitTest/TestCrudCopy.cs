﻿using Backend.Data;
using Backend.Database;
using Backend.Logic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Backend.Tests.UnitTest
{
    [TestClass]
    public class TestCrudCopy
    {
        private const int CopyId = 56789654;
        private const string BookIsbn = "346895674";
        private Mock<IDbContext> _mockDbContext;
        private Mock<IFactory<IDbContext>> _mockIFactory;
        private Mock<IBookControllerInternal> _mockBookController;
        private ICopyController _copyController;
        private Copy _copy;
        private Book _book;

        [TestInitialize]
        public void Initialize()
        {
            _mockIFactory = new Mock<IFactory<IDbContext>>();
            _mockDbContext = new Mock<IDbContext>();
            _mockIFactory.Setup(f => f.Create()).Returns(_mockDbContext.Object);
            _mockBookController = new Mock<IBookControllerInternal>();

            _copyController = new CopyController(_mockIFactory.Object, _mockBookController.Object);

            _book = new Book {Isbn10 = BookIsbn};
            _copy = new Copy
            {
                Id = CopyId,
                Book = _book
            };
        }

        [TestCleanup]
        public void Cleanup()
        {

        }

        [TestMethod]
        public void Get_Copy_Positive()
        {
            _mockDbContext.Setup(context => context.Copys.Find(CopyId)).Returns(_copy);
            //_mockDbContext.Verify(context => context.Copys.Find(), Times.Once); //TODO: Find out why this won't work.

            Copy result = _copyController.GetCopy(CopyId);
            
            Assert.IsNotNull(result);
            Assert.AreEqual(CopyId, result.Id);
        }

        [TestMethod]
        public void Create_Copy_Positive()
        {
            _mockDbContext.Setup(context => context.Copys.Add(It.IsAny<Copy>())).Returns<Copy>(copy =>
            {
                copy.Id = CopyId;
                return copy;
            });
            _mockBookController.Setup(context => context.GetBook(_mockDbContext.Object, BookIsbn)).Returns(_book);

            Copy result = _copyController.CreateCopy(BookIsbn);

            _mockDbContext.Verify(context => context.Copys.Add(It.IsAny<Copy>()), Times.Once);
            _mockDbContext.Verify(context => context.SaveChanges(), Times.Once);
            Assert.IsNotNull(result);
            Assert.AreEqual(CopyId, result.Id);
        }
    }
}
