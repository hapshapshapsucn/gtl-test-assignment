﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using Backend.Data;

namespace Backend.Tests.Mocking
{
    public class FakeLoanSet : FakeDbSetMoq<Loan>
    {
        public override Loan Find(params object[] keyValues)
        {
            return this.SingleOrDefault(d => d.LoanNumber == (int)keyValues.Single());
        }
    }

    public class FakeDbSetMoq<T> : IDbSet<T> where T : class
    {
        ObservableCollection<T> _data;
        IQueryable _query;

        public FakeDbSetMoq()
        {
            _data = new ObservableCollection<T>();
            _query = _data.AsQueryable();
        }

        public virtual T Find(params object[] keyValues)
        {
            throw new NotImplementedException("Derive from FakeDbSet<T> and override Find");
        }

        public virtual T Add(T item)
        {
            _data.Add(item);
            return item;
        }

        public virtual T Remove(T item)
        {
            _data.Remove(item);
            return item;
        }

        public virtual T Attach(T item)
        {
            _data.Add(item);
            return item;
        }

        public virtual T Detach(T item)
        {
            _data.Remove(item);
            return item;
        }

        public virtual T Create()
        {
            return Activator.CreateInstance<T>();
        }

        public virtual TDerivedEntity Create<TDerivedEntity>() where TDerivedEntity : class, T
        {
            return Activator.CreateInstance<TDerivedEntity>();
        }

        public virtual ObservableCollection<T> Local
        {
            get { return _data; }
        }

        Type  IQueryable.ElementType
        {
            get { return _query.ElementType; }
        }

        System.Linq.Expressions.Expression IQueryable.Expression
        {
            get { return _query.Expression; }
        }

        IQueryProvider IQueryable.Provider
        {
            get { return _query.Provider; }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _data.GetEnumerator();
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return _data.GetEnumerator();
        }
    }
}