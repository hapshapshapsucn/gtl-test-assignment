﻿using System;
using Backend.Data;
using Backend.Database;

namespace TestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Initializing db");
            using (EfDbContext db = new EfDbContext("Data Source = localhost; Initial Catalog = TestDb; Integrated Security = True; App = GTLTes"))
            {
                //db.Database.Create();

                db.Members.Add(new Member
                {
                    Adress = "Asd'f",
                    Campus = "dasf'",
                    Name = "Hillbil'ly",
                    PhoneNumber = "a'sdfghj",
                    SSN = "N'(ewrtyjy423'13)"
                });

                db.SaveChanges();
            }
        }
    }
}
