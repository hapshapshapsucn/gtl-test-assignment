﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Backend
{
    public static class Utility
    {
        public enum IsbnType
        {
            Isbn10 = 10,
            Isbn13 = 13,
            None
        }

        public static string RandomString(int length)
        {
            // The code is from here: http://stackoverflow.com/a/1344255
            // And it works way better than previously tried examples.
            char[] chars = new char[62];
            chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
            byte[] data = new byte[1];
            using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetNonZeroBytes(data);
                data = new byte[length];
                crypto.GetNonZeroBytes(data);
            }
            StringBuilder result = new StringBuilder(length);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result.ToString();
        }

        #region ISBN_Validation

        public static IsbnType GetIsbnType(string isbn)
        {
            if (String.IsNullOrEmpty(isbn))
                return IsbnType.None;
            //isbn = SimplifyIsbn(isbn);
            var arrayOfInts = isbn.Select(character => int.Parse(character.ToString())).ToArray();
            if (isbn.Length == 10 && Validate10(arrayOfInts))
            {
                return IsbnType.Isbn10;
            }
            if (isbn.Length == 13 && Validate13(arrayOfInts))
            {
                return IsbnType.Isbn13;
            }
            //throw new Exception("ISBN is not 10 or 13 digits");
            return IsbnType.None;
        }

        public static string SimplifyIsbn(string isbn)
        {
            if (String.IsNullOrEmpty(isbn)) return isbn;
            IList<int> listOfInt = new List<int>();
            isbn = isbn.Trim();
            isbn = isbn.Replace("-", "");
            isbn = isbn.Replace(" ", "");
            isbn = RemoveAllNonValidChars(isbn, "0123456789X".ToCharArray());
            if (String.IsNullOrEmpty(isbn)) return isbn;
            foreach (char c in isbn)
            {
                if (c == '0' || c == 'X')
                    listOfInt.Add(0);
                else
                {
                    listOfInt.Add(int.Parse(c.ToString()));
                }
            }
            return String.Concat(listOfInt);
        }

        public static string RemoveAllNonValidChars(string input, char[] validChars)
        {
            string returnvalue = "";
            foreach (var s in input)
            {
                if (validChars.Contains(s))
                    returnvalue += s;
            }
            if (returnvalue == "")
                returnvalue = null;
            return returnvalue;
        }

        public static bool ValidateIsbn(string isbn)
        {
            bool returnValue = false;
            isbn = SimplifyIsbn(isbn);
            if (GetIsbnType(isbn) != IsbnType.None) returnValue = true;

            return returnValue;
        }

        private static bool Validate10(int[] isbn)
        {
            int sum = 0;
            for (int i = 10; i != 1; i--)
            {
                sum += isbn[10 - i] * i;
            }
            int remainder = (sum % 11);
            int check = 11 - remainder;
            if (check == 10)
            {
                check = 0;
            }
            return check == isbn[isbn.Length - 1];
        }

        private static bool Validate13(int[] isbn)
        {
            int checkSum = CalculateIsbn13(isbn);
            return checkSum == isbn[isbn.Length - 1];
        }

        private static int CalculateIsbn13(int[] isbn)
        {
            //if (isbn.Length == 12)
            //{
                int sum = 0;
                for (int i = 0; i != 12; i++)
                {
                    if (i % 2 == 0)
                        sum += isbn[i];
                    else
                        sum += isbn[i] * 3;
                }
                int remainder = sum % 10;
                int checkSum = 10 - remainder;
                if (checkSum == 10)
                    checkSum = 0;
                return checkSum;
            //}
            //return -1;
        }

        #endregion


        //#region Sanitizer

        ///// <summary>
        ///// Takes an object as input which can either be of the type Project and Task. 
        ///// Sanitizes everything within given object and returns a safe to execute to sql version
        ///// </summary>
        ///// <param name="input"></param>
        ///// <returns>Project or Task which is safe to execute variables against sql server</returns>
        //public static object Sanitizer(object input)
        //{
        //    foreach (PropertyInfo p in input.GetType().GetProperties())
        //    {
        //        if (p.PropertyType == typeof(string))
        //        {
        //            //if (!string.IsNullOrEmpty(p.GetValue(input, null).ToString()))
        //            //if(p.CanRead && p.CanWrite)
        //            if (Nullable.GetUnderlyingType(p.PropertyType) != null)
        //            {
        //                p.SetValue(input, Sanitize(p.GetValue(input, null).ToString()));
        //            }
        //        }
        //    }
        //    return input;
        //}

        //public static object Desanitizer(object input)
        //{
        //    foreach (PropertyInfo p in input.GetType().GetProperties())
        //    {
        //        if (p.PropertyType == typeof(string))
        //        {
        //            if (Nullable.GetUnderlyingType(p.PropertyType) != null)
        //            {
        //                p.SetValue(input, Desanitize(p.GetValue(input, null).ToString()));
        //            }
        //        }
        //    }
        //    return input;
        //}

        //public static string Sanitize(string s)
        //{
        //    if (s.Contains("'"))
        //        return s.Replace("'", "''");
        //    return s;
        //}

        //public static string Desanitize(string s)
        //{
        //    if (s.Contains("''"))
        //        return s.Replace("''", "'");
        //    return s;
        //}

        //#endregion
    }
}