﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using Backend.Data;

namespace Backend.Database
{
    public interface IDbContext : IDisposable
    {
        IDbSet<Book> Books { get; set; }
        IDbSet<Loan> Loans { get; set; }
        IDbSet<Copy> Copys { get; set; }
        IDbSet<Member> Members { get; set; }
        IDbSet<Professor> Professors { get; set; }
        int SaveChanges();
        Task<int> SaveChangesAsync();
    }
}
