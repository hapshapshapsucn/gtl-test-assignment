﻿using System;
using System.Data.Common;
using System.Linq;
using Backend.Data;

namespace Backend.Database
{
    public enum ApplicationScenario
    {
        Production,
        TestingTransient,
        TestingPersistent,
        Development
    }

    public interface IFactory<out T>
    {
        T Create();
    }

    internal class DbContextFactory : IFactory<IDbContext>
    {
        private readonly string _dbPersistentId;
        private readonly ApplicationScenario _scenario;
        private readonly string _connectionStringEnvi;
        private bool _dbDataIsGenerated;
        public DbContextFactory(ApplicationScenario scenario)
        {
            _scenario = scenario;
            Console.WriteLine(Environment.CurrentDirectory);
            _connectionStringEnvi = Environment.GetEnvironmentVariable("GTLSQLConnectionString");

            if (scenario == ApplicationScenario.TestingPersistent)
            {
                _dbPersistentId = Utility.RandomString(15);
            }
        }

        public IDbContext Create()
        {
            IDbContext returnValue;
            DbConnection dbConnection;

            switch (_scenario)
            {
                case ApplicationScenario.Production:
                    if (_connectionStringEnvi != null)
                    {
                        returnValue = new EfDbContext(_connectionStringEnvi);
                    }
                    else
                    {
                        throw new Exception("No environment variabel for 'GTLSQLConnectionString' is defined for this production scenario");
                        //returnValue = new EfDbContext("name=LocalDatabase"); // The database catalog will be "GeorgiaTechLibrary".
                        //returnValue = new EfDbContext("name=Kraka");
                    }
                    if (!_dbDataIsGenerated) { GenerateDataToDb(returnValue); }
                    break;
                case ApplicationScenario.TestingTransient:
                    dbConnection = Effort.DbConnectionFactory.CreateTransient();
                    returnValue = new EfDbContext(dbConnection);
                    break;
                case ApplicationScenario.TestingPersistent:
                    dbConnection = Effort.DbConnectionFactory.CreatePersistent(_dbPersistentId);
                    returnValue = new EfDbContext(dbConnection);
                    break;
                case ApplicationScenario.Development:
                    dbConnection = Effort.DbConnectionFactory.CreatePersistent("4oi8y3do!u74ewtdfghjop");
                    returnValue = new EfDbContext(dbConnection);
                    if (!_dbDataIsGenerated) { GenerateDataToDb(returnValue); }
                    break;
                default:
                    throw new ApplicationException("The scenario: " + _scenario + " is not defined in the Factory.");
            }
            return returnValue;
        }

        private void GenerateDataToDb(IDbContext db)
        {
            if (db.Books.Any())
            {
                return;
            }
            //Book
            db.Books.Add(new Book { Isbn10 = "0671629646", Title = "Hitchikers Guide to the galaxy" });
            db.Books.Add(new Book { Isbn13 = "9781408044056", Title = "Foundations of Software Testing ISTQB Certification" });
            db.Books.Add(new Book { Isbn10 = "1631409115", Isbn13 = "9781631409110", Title = "Donald Duck - Timeless Tales" });
            db.Books.Add(new Book { Isbn10 = "8789191641", Isbn13 = "9788789191645", Title = "Da landet forsvandt", SubTitle = "et spil hvor du selv vælger side" });
            db.Books.Add(new Book { Isbn10 = "8793244541", Isbn13 = "9788793244542", Title = "Splint & Co.- 1988-1991" });
            db.SaveChanges();
            foreach (var book in db.Books)
            {
                for (int i = 0; i < 5; i++)
                {
                    var copy = new Copy
                    {
                        Book = book
                    };
                    db.Copys.Add(copy);
                }
            }
            db.Members.Add(new Member
            {
                SSN = "98erihfd89d",
                Adress = "1407 Graymalkin Lane, Salem Center",
                Campus = "The Xavier's School for Gifted Youngsters (X-Mansion)",
                Name = "Scott Summers (Cyclops)",
                PhoneNumber = "212-576-4000"
            });
            db.Members.Add(new Member
            {
                SSN = "6857tjterd0fj9d",
                Adress = "1407 Graymalkin Lane, Salem Center",
                Campus = "The Xavier's School for Gifted Youngsters (X-Mansion)",
                Name = "Jean Grey (Phoenix)",
                PhoneNumber = "212-576-4000"
            });
            db.Members.Add(new Member
            {
                SSN = "5u6bjr9sbgrd89d",
                Adress = "1407 Graymalkin Lane, Salem Center",
                Campus = "The Xavier's School for Gifted Youngsters (X-Mansion)",
                Name = "James 'Jimmy' Howlett (Logan, Wolverine)",
                PhoneNumber = "212-576-4000"
            });
            db.Professors.Add(new Professor
            {
                SSN = "2uj46ute4ud",
                Adress = "1407 Graymalkin Lane, Salem Center",
                Campus = "The Xavier's School for Gifted Youngsters (X-Mansion)",
                Name = "Professor Charles Francis Xavier",
                PhoneNumber = "212-576-4000"
            });
            db.SaveChanges();
            //Done
            _dbDataIsGenerated = true;
        }
    }
}
