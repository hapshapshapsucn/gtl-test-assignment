﻿using System.Data.Common;
using System.Data.Entity;
using Backend.Data;

namespace Backend.Database
{
    public class EfDbContext : DbContext, IDbContext
    {
        public EfDbContext(string connectionStringOrName) : base(connectionStringOrName)
        {

        }

        public EfDbContext(DbConnection dbConnection) : base(dbConnection, true)
        {

        }


        public IDbSet<Book> Books { get; set; }
        public IDbSet<Loan> Loans { get; set; }
        public IDbSet<Author> Authors { get; set; }
        public IDbSet<Copy> Copys { get; set; }
        public IDbSet<Member> Members { get; set; }
        public IDbSet<Professor> Professors { get; set; }





        #region DbContext info
        // Your context has been configured to use a 'EfDataContext' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'Backend.EfDataContext' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'EfDataContext' 
        // connection string in the application configuration file.
        //public EfDataContext()
        //    : base("name=EfDataContext")
        //{
        //}

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }

        #endregion
    }
}
