﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Backend.Data;
using Backend.Database;

namespace Backend.Logic
{
    internal class LoanController : ILoanControllerInternal
    {
        private readonly IFactory<IDbContext> _dbContextFactory;
        private readonly ICopyControllerInternal _copyController;
        private readonly IMemberControllerInternal _memberController;
        private enum MaxLoans
        {
            Member = 5
        }
        private enum LoanTime
        {
            Member = 21,
            Professor = 90
        }

        public LoanController(IFactory<IDbContext> dbContextFactory, ICopyControllerInternal copyController, IMemberControllerInternal memberController)
        {
            _dbContextFactory = dbContextFactory;
            _copyController = copyController;
            _memberController = memberController;
        }

        public Loan CreateLoan(IDbContext dbContext, int loanedCopyId, string memberSsn)
        {
            Loan returnLoan = null;
            Copy copy = _copyController.GetCopy(dbContext, loanedCopyId);
            Member member = _memberController.GetMember(dbContext, memberSsn);
            DateTime loanEndDateTime = member is Professor ? DateTime.Now.AddDays((int)LoanTime.Professor) : DateTime.Now.AddDays((int)LoanTime.Member);
            Loan getLoan = GetLoanByCopyId(loanedCopyId);
            var loans = GetLoans(memberSsn);
            if (copy != null && member != null && (getLoan?.LoanDone ?? true) && (loans == null || loans.Count < (int)MaxLoans.Member))
            {
                Loan loan = new Loan
                {
                    CreationDateTime = DateTime.Now,
                    EndDateTime = loanEndDateTime,
                    LoanedCopy = copy,
                    Member = member
                };
                returnLoan = dbContext.Loans.Add(loan);
            }
            return returnLoan;
        }

        public Loan CreateLoan(int loanedCopyId, string memberSsn)
        {
            Loan loan;
            using (IDbContext db = _dbContextFactory.Create())
            {
                loan = CreateLoan(db, loanedCopyId, memberSsn);
                db.SaveChanges();
            }
            return loan;
        }

        public Loan GetLoan(IDbContext dbContext, int loanNumber)
        {
            Loan loan = dbContext.Loans.Find(loanNumber);
            if (loan != null)
            {
                loan.LoanedCopy = _copyController.GetCopy(dbContext, loan.LoanedCopy.Id);
            }
            return loan;
        }

        public Loan GetLoan(int loanNumber)
        {
            Loan loan;
            using (IDbContext db = _dbContextFactory.Create())
            {
                loan = GetLoan(db, loanNumber);
            }
            return loan;
        }

        public List<Loan> GetLoans(IDbContext db, string ssn)
        {
            List<Loan> returnValue = new List<Loan>();
            List<int> loanids = db.Loans.Where(loan => loan.Member.SSN == ssn).Select(id => id.LoanNumber).ToList();
            loanids.ForEach(loanId => returnValue.Add(GetLoan(db, loanId)));
            if (returnValue.Count < 1)
                returnValue = null;
            return returnValue;
        }

        public List<Loan> GetLoans(string ssn)
        {
            List<Loan> returnValue = new List<Loan>();
            using (IDbContext db = _dbContextFactory.Create())
            {
                returnValue = GetLoans(db, ssn);
            }
            return returnValue;
        }

        public Loan GetLoanByCopyId(IDbContext dbContext, int copyId)
        {
            Loan loan = dbContext.Loans.FirstOrDefault(n => n.LoanedCopy.Id == copyId);
            return loan;
        }

        public Loan GetLoanByCopyId(int copyId)
        {
            Loan loan;
            using (IDbContext db = _dbContextFactory.Create())
            {
                loan = GetLoanByCopyId(db, copyId);
            }
            return loan;
        }

        public List<Copy> GetCopysWithoutActiveLoan(IDbContext db, string bookIsbn)
        {
            var copies = from copy in db.Copys
                         join loan in db.Loans on copy equals loan.LoanedCopy into lo
                         from loan in lo.DefaultIfEmpty()
                         where copy.Book.Isbn10 == bookIsbn && (copy.Id != loan.LoanedCopy.Id || loan.LoanDone)
                         select copy;
            return !copies.Any() ? null : copies.ToList();
        }

        public List<Copy> GetCopysWithoutActiveLoan(string bookIsbn)
        {
            List<Copy> returnValue;
            using (IDbContext db = _dbContextFactory.Create())
            {
                returnValue = GetCopysWithoutActiveLoan(db, bookIsbn);
            }
            return returnValue;
        }

        public int CountCopysWithoutActiveLoan(string bookIsbn)
        {
            int returnValue = 0;
            using (IDbContext db = _dbContextFactory.Create())
            {
                var copies = from copy in db.Copys
                join loan in db.Loans on copy equals loan.LoanedCopy into lo
                from loan in lo.DefaultIfEmpty()
                where copy.Book.Isbn10 == bookIsbn && (copy.Id != loan.LoanedCopy.Id || loan.LoanDone)
                select copy;
                returnValue = copies.Count();
            }
            return returnValue;
            //return !copies.Any() ? null : copies.ToList();
        }

        public Loan UpdateLoan(IDbContext dbContext, int loanNumber, DateTime newEndDateTime)
        {
            throw new NotImplementedException();
        }

        public Loan UpdateLoan(int loanNumber, DateTime newEndDateTime)
        {
            throw new NotImplementedException();
        }

        public bool RemoveLoan(IDbContext dbContext, int loanNumber)
        {
            throw new NotImplementedException();
        }

        public bool RemoveLoan(int loanNumber)
        {
            throw new NotImplementedException();
        }

        public bool EndLoan(IDbContext dbContext, int loanNumber)
        {
            var loan = GetLoan(dbContext, loanNumber);
            loan.LoanDone = true;
            Member member = _memberController.GetMember(dbContext, "98erihfd89d");
            loan.Member = member;
            dbContext.SaveChanges();
            return loan.LoanDone;
        }

        public bool EndLoan(int loanNumber)
        {
            using (IDbContext db = _dbContextFactory.Create())
            {
                return EndLoan(db, loanNumber);
            }
        }
    }
}