﻿using System;
using System.Collections.Generic;
using Backend.Data;
using Backend.Database;

namespace Backend.Logic
{
    public interface ILoanController
    {
        Loan CreateLoan(int loanedCopyId, string memberSsn); //Create
        Loan GetLoan(int loanNumber); //Read
        List<Loan> GetLoans(string ssn);
        Loan GetLoanByCopyId(int copyId);
        List<Copy> GetCopysWithoutActiveLoan(string bookIsbn);
        int CountCopysWithoutActiveLoan(string bookIsbn);
        Loan UpdateLoan(int loanNumber, DateTime newEndDateTime); //Update
        bool RemoveLoan(int loanNumber); //Delete
        bool EndLoan(int loanNumber);
    }

    internal interface ILoanControllerInternal : ILoanController
    {
        Loan CreateLoan(IDbContext dbContext, int loanedCopyId, string memberSsn); //Create
        Loan GetLoan(IDbContext dbContext, int loanNumber); //Read
        List<Loan> GetLoans(IDbContext db, string ssn);
        Loan GetLoanByCopyId(IDbContext dbContext, int copyId);
        List<Copy> GetCopysWithoutActiveLoan(IDbContext db, string bookIsbn);
        Loan UpdateLoan(IDbContext dbContext, int loanNumber, DateTime newEndDateTime); //Update
        bool RemoveLoan(IDbContext dbContext, int loanNumber); //Delete
        bool EndLoan(IDbContext dbContext, int loanNumber);
    }
}
