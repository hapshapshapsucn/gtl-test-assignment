﻿using System;
using System.Collections.Generic;
using System.Linq;
using Backend.Data;
using Backend.Database;

namespace Backend.Logic
{
    internal class BookController : IBookControllerInternal
    {
        private readonly IFactory<IDbContext> _dbContextFactory;
        //private Func<IDbContext> _dbContextFunc;
        public BookController(IFactory<IDbContext> dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Book CreateBook(IDbContext dbContext, string isbn13, string isbn10, string title, string subTitle, Author author, int publicationYear, int pageCount)
        {
            isbn13 = Utility.SimplifyIsbn(isbn13);
            isbn10 = Utility.SimplifyIsbn(isbn10);
            var isbnType13 = Utility.GetIsbnType(isbn13);
            var isbnType10 = Utility.GetIsbnType(isbn10);
            if (isbnType13 == Utility.IsbnType.None && isbnType10 == Utility.IsbnType.None)
                return null;
            Book book = new Book
            {
                Title = title,
                SubTitle = subTitle,
                PublicationYear = publicationYear,
                PageCount = pageCount
                //Author = author //TODO: Add auther, and make sure the auther exists. Or pehaps just add the auther to the db?
            };
            if (isbnType13 == Utility.IsbnType.Isbn13)
                book.Isbn13 = isbn13;
            if (isbnType10 == Utility.IsbnType.Isbn10)
                book.Isbn10 = isbn10;
            Book returnBook = dbContext.Books.Add(book);
            return returnBook;
        }

        public Book CreateBook(string isbn13, string isbn10, string title, string subTitle, Author author, int publicationYear, int pageCount)
        {
            Book book;
            //TODO: Check if auther exist in db. Should it add them if it doesn't exist?
            using (IDbContext db = _dbContextFactory.Create() /*_dbContextFactory.Create()*/)
            {
                book = CreateBook(db, isbn13, isbn10, title, subTitle, author, publicationYear, pageCount);
                db.SaveChanges();
                //throw new NotImplementedException();
            }
            return book;
        }

        public Book GetBook(IDbContext dbContext, int id)
        {
            return dbContext.Books.Find(id);
        }

        public Book GetBook(int id)
        {
            Book book;
            using (IDbContext db = _dbContextFactory.Create())
            {
                book = GetBook(db, id);
            }
            return book;
        }

        public Book GetBook(IDbContext dbContext, string isbn)
        {
            Book book = null;
            isbn = Utility.SimplifyIsbn(isbn);
            var isbnType = Utility.GetIsbnType(isbn);
            if (isbnType == Utility.IsbnType.Isbn10)
            {
                book = dbContext.Books.FirstOrDefault(b => b.Isbn10 == isbn);
            }
            else if (isbnType == Utility.IsbnType.Isbn13)
            {
                book = dbContext.Books.FirstOrDefault(b => b.Isbn13 == isbn);
            }
            return book;
        }

        public Book GetBook(string isbn)
        {
            Book book;
            using (IDbContext db = _dbContextFactory.Create())
            {
                book = GetBook(db, isbn);
            }
            return book;
        }

        public List<Book> GetBooks(string searchTitle)
        {
            List<Book> bookList;
            using (IDbContext db = _dbContextFactory.Create())
            {
                bookList = db.Books.Where(book => book.Title.Contains(searchTitle)).ToList();
            }
            return bookList;
        }

        public List<Book> GetBooksAmount(int amount)
        {
            List<Book> returnValue = new List<Book>();
            using (IDbContext db = _dbContextFactory.Create())
            {
                List<int> bookIds = db.Books.Take(amount).Select(book => book.Id).ToList();
                bookIds.ForEach(bookid => returnValue.Add(GetBook(db, bookid)));
            }
            return returnValue;
        }

        public List<Book> GetAllBooks(IDbContext db)
        {
            var returnValue = new List<Book>();
            List<int> bookIds = db.Books.Select(book => book.Id).ToList();
            bookIds.ForEach(bookid => returnValue.Add(GetBook(db, bookid)));
            return returnValue;
        }

        public List<Book> GetAllBooks()
        {
            List<Book> returnValue;
            using (IDbContext db = _dbContextFactory.Create())
            {
                returnValue = GetAllBooks(db);
            }
            return returnValue;
        }

        public Book UpdateBook(IDbContext dbContext, string isbn, string newIsbn, string newTitle, Author newAuthor,
            DateTime newPublicationYear)
        {
            Book book = GetBook(dbContext, isbn);
            if (book != null)
            {
                book.Isbn10 = newIsbn;
                book.Title = newTitle;
                //Book.Auther = newAuthor;
                dbContext.SaveChanges(); //TODO: Do something with the returned int.
            }
            return book;
        }

        public Book UpdateBook(string isbn, string newIsbn, string newTitle, Author newAuthor, DateTime newPublicationYear)
        {
            Book book;
            using(IDbContext db = _dbContextFactory.Create())
            {
                book = UpdateBook(db, isbn, newIsbn, newTitle, newAuthor, newPublicationYear);
            }
            return book;
        }

        public bool RemoveBook(IDbContext dbContext, string isbn)
        {
            bool returnValue = false;
            Book book = GetBook(isbn);
            if (book != null)
            {
                dbContext.Books.Remove(book);
                returnValue = true;
            }
            return returnValue;
        }

        public bool RemoveBook(string isbn)
        {
            bool returnValue = false;
            using (IDbContext db = _dbContextFactory.Create())
            {
                returnValue = RemoveBook(db, isbn);
                db.SaveChanges();
            }
            return returnValue;
        }
    }
}
