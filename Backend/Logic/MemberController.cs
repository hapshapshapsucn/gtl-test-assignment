﻿using Backend.Data;
using Backend.Database;

namespace Backend.Logic
{
    internal class MemberController : IMemberControllerInternal
    {
        private readonly IFactory<IDbContext> _dbContextFactory;

        public MemberController(IFactory<IDbContext> dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public Member CreateMember(string ssn, string name, string campus, string adress, string phoneNumber)
        {
            Member member; 
            using (IDbContext dbContext = _dbContextFactory.Create())
            {
                member = dbContext.Members.Add(new Member
                {
                    SSN = ssn,
                    Name = name,
                    Campus = campus,
                    Adress = adress,
                    PhoneNumber = phoneNumber
                });
                dbContext.SaveChanges();
            }
            return member;
        }

        public Member GetMember(IDbContext dbContext, string ssn)
        {
            Member member = dbContext.Members.Find(ssn);
            return member;
        }

        public Member GetMember(string ssn)
        {
            Member member;
            using (IDbContext db = _dbContextFactory.Create())
            {
                member = GetMember(db, ssn);
            }
            return member;
        }

        public Member UpdateMember(string ssn, string newSsn, string newName, string newCampus, string newAdress, string newPhoneNumber)
        {
            throw new System.NotImplementedException();
        }

        public bool RemoveMember(string ssn)
        {
            throw new System.NotImplementedException();
        }

        public Member CreateMember(IDbContext dbContext, string ssn, string name, string campus, string adress, string phoneNumber)
        {
            throw new System.NotImplementedException();
        }

        public Member UpdateMember(IDbContext dbContext, string ssn, string newSsn, string newName, string newCampus, string newAdress,
            string newPhoneNumber)
        {
            throw new System.NotImplementedException();
        }

        public bool RemoveMember(IDbContext dbContext, string ssn)
        {
            throw new System.NotImplementedException();
        }
    }
}