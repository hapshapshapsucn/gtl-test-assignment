﻿using System.Collections.Generic;
using Backend.Data;
using Backend.Database;

namespace Backend.Logic
{
    public interface ICopyController
    {
        Copy CreateCopy(string bookIsbn); //Create retunere copy med det genererede id.
        Copy GetCopy(int copyId); //Read retunere copy med et nyt genereret id.
        List<Copy> GetCopies(int bookId);

        int CountCopies(int bookId);
        Copy UpdateCopy(int copyId); //Update
        bool RemoveCopy(int copyId); //Delete
    }

    internal interface ICopyControllerInternal : ICopyController
    {
        Copy CreateCopy(IDbContext dbContext, string bookIsbn); //Create retunere copy med det genererede id.
        Copy GetCopy(IDbContext dbContext, int copyId); //Read retunere copy med et nyt genereret id.

        List<Copy> GetCopies(IDbContext db, int bookId);
        Copy UpdateCopy(IDbContext dbContext, int copyId); //Update
        bool RemoveCopy(IDbContext dbContext, int copyId); //Delete
    }
}
