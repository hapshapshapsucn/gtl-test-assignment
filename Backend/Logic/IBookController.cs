﻿using System;
using System.Collections.Generic;
using Backend.Data;
using Backend.Database;

namespace Backend.Logic
{
    public interface IBookController
    {
        Book CreateBook(string isbn13, string isbn10, string title, string subTitle, Author author, int publicationYear, int pageCount); //Create
        Book GetBook(int id);
        Book GetBook(string isbn); //Read
        List<Book> GetBooks(string searchTitle);
        List<Book> GetBooksAmount(int amount);
        List<Book> GetAllBooks();
        Book UpdateBook(string isbn, string newId, string newTitle, Author newAuthor, DateTime newPublicationYear); //Update
        bool RemoveBook(string isbn); //Delete
    }

    internal interface IBookControllerInternal : IBookController
    {
        Book CreateBook(IDbContext dbContext, string isbn13, string isbn10, string title, string subTitle, Author author, int publicationYear, int pageCount);
        Book GetBook(IDbContext dbContext, int id);
        Book GetBook(IDbContext dbContext, string isbn); //Read
        List<Book> GetAllBooks(IDbContext db);
        Book UpdateBook(IDbContext dbContext, string isbn, string newId, string newTitle, Author newAuthor, DateTime newPublicationYear); //Update
        bool RemoveBook(IDbContext dbContext, string isbn); //Delete
    }
}
