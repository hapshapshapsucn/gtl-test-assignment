﻿using System.Collections.Generic;
using System.Linq;
using Backend.Data;
using Backend.Database;

namespace Backend.Logic
{
    internal class CopyController : ICopyControllerInternal
    {
        private readonly IFactory<IDbContext> _dbContextFactory;
        private readonly IBookControllerInternal _bookController;

        public CopyController(IFactory<IDbContext> dbContextFactory, IBookControllerInternal bookController)
        {
            _dbContextFactory = dbContextFactory;
            _bookController = bookController;
        }

        public Copy CreateCopy(IDbContext dbContext, string bookIsbn)
        {
            Copy returnValue = null;
            Copy copy = new Copy { Book = _bookController.GetBook(dbContext, bookIsbn) };
            if (copy.Book != null)
            {
                Copy returnCopy = dbContext.Copys.Add(copy); //TODO: Add relation to the given Book.
                returnValue = returnCopy;
            }
            return returnValue;
        }

        public Copy CreateCopy(string bookIsbn)
        {
            Copy returnCopy;
            using (IDbContext db = _dbContextFactory.Create())
            {
                returnCopy = CreateCopy(db, bookIsbn);
                db.SaveChanges();
            }
            return returnCopy;
        }

        public Copy GetCopy(IDbContext dbContext, int copyId)
        {
            Copy copy = dbContext.Copys.Find(copyId);
            if (copy != null)
            {
                copy.Book = _bookController.GetBook(dbContext, copy.Book.Id);
            }
            return copy;
        }

        public Copy GetCopy(int copyId)
        {
            Copy copy;
            using (IDbContext db = _dbContextFactory.Create())
            {
                copy = GetCopy(db, copyId);
            }
            return copy;
        }

        public List<Copy> GetCopies(IDbContext db, int bookId)
        {
            var returnValue = new List<Copy>();
            foreach (var copy in db.Copys.Where(copy => copy.Book.Id == bookId))
            {
                Copy c = GetCopy(copy.Id);
                returnValue.Add(c);
            }
            if (returnValue.Count < 1)
            {
                returnValue = null;
            }
            return returnValue;
        }

        public List<Copy> GetCopies(int bookId)
        {
            List<Copy> copies;
            using (IDbContext db = _dbContextFactory.Create())
            {
                copies = GetCopies(db, bookId);
            }
            return copies;
        }

        public int CountCopies(int bookId)
        {
            int returnValue = 0;
            using (IDbContext db = _dbContextFactory.Create())
            {
                returnValue = db.Copys.Count(copy => copy.Book.Id == bookId);
            }
            return returnValue;
        }

        public Copy UpdateCopy(int copyId)
        {
            throw new System.NotImplementedException();
        }

        public bool RemoveCopy(int copyId)
        {
            throw new System.NotImplementedException();
        }

        public Copy UpdateCopy(IDbContext dbContext, int copyId)
        {
            throw new System.NotImplementedException();
        }

        public bool RemoveCopy(IDbContext dbContext, int copyId)
        {
            throw new System.NotImplementedException();
        }
    }
}