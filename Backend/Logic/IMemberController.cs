﻿using Backend.Data;
using Backend.Database;

namespace Backend.Logic
{
    public interface IMemberController
    {
        Member CreateMember(string ssn, string name, string campus, string adress, string phoneNumber); //Create
        Member GetMember(string ssn); //Read
        Member UpdateMember(string ssn, string newSsn, string newName, string newCampus, string newAdress, string newPhoneNumber); //Update
        bool RemoveMember(string ssn); //Delete
    }

    internal interface IMemberControllerInternal : IMemberController
    {
        Member CreateMember(IDbContext dbContext, string ssn, string name, string campus, string adress, string phoneNumber); //Create
        Member GetMember(IDbContext dbContext, string ssn); //Read
        Member UpdateMember(IDbContext dbContext, string ssn, string newSsn, string newName, string newCampus, string newAdress, string newPhoneNumber); //Update
        bool RemoveMember(IDbContext dbContext, string ssn); //Delete
    }
}
