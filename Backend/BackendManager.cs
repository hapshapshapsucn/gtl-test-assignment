﻿using System.Diagnostics.CodeAnalysis;
using Backend.Database;
using Backend.Logic;

namespace Backend
{
    public class BackendManager
    {
        //Fields
        private readonly IMemberControllerInternal _memberController;
        private readonly IBookControllerInternal _bookController;
        private readonly ICopyControllerInternal _copyController;
        private readonly ILoanControllerInternal _loanController;

        //Properties
        public IMemberController MemberController => _memberController;
        public IBookController BookController => _bookController;
        public ICopyController CopyController => _copyController;
        public ILoanController LoanController => _loanController;

        public BackendManager()
        {
            IFactory<IDbContext> dbFactory = GetDbContext();
            _memberController = new MemberController(dbFactory);
            _bookController = new BookController(dbFactory);
            _copyController = new CopyController(dbFactory, _bookController);
            _loanController = new LoanController(dbFactory, _copyController, _memberController);
        }

        [SuppressMessage("ReSharper", "JoinDeclarationAndInitializer")]
        private IFactory<IDbContext> GetDbContext()
        {
            IFactory<IDbContext> returnValue;
#if DEBUG
            returnValue = new DbContextFactory(ApplicationScenario.Development);
#else
            returnValue = new DbContextFactory(ApplicationScenario.Production);
#endif
            return returnValue;
        }
    }
}
