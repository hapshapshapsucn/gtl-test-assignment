﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Backend.Data
{
    public class Copy
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Index]
        [Required]
        public virtual Book Book { get; set; }
        [NotMapped]
        public string BookCopyId => (Book.Isbn13 ?? Book.Isbn10) + "c" + Id;
    }
}
