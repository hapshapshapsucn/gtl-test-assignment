﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Backend.Data
{
    public class Book
    {
        //[Key]
        //public int Id { get; set; }
        //[Index(IsUnique = true)]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Isbn10 { get; set; }
        public string Isbn13 { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public Author Author { get; set; }
        //[NotMapped]
        public int PublicationYear { get; set; }
        public int PageCount { get; set; }

        public override string ToString()
        {
            return String.Format("ISBN: {0}, Title: {1}, SubTitle: {2}, Publication year: {3}, PageCount: {4}", (Isbn13 ?? Isbn10), Title, SubTitle, PublicationYear, PageCount);
        }
    }
}
