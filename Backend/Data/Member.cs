﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Backend.Data
{
    public class Member
    {
        [Key/*, Index(IsUnique = true)*/]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string SSN { get; set; }
        public string Name { get; set; }
        public string Campus { get; set; }
        public string Adress { get; set; } //Home adress. Not e-mail.
        public string PhoneNumber { get; set; }
    }
}
