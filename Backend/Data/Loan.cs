﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Backend.Data
{
    public class Loan
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LoanNumber { get; set; } //Id
        public DateTime CreationDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        [Required]
        public virtual Copy LoanedCopy { get; set; }
        [Required]
        public virtual Member Member { get; set; }
        public bool LoanDone { get; set; }
    }
}
